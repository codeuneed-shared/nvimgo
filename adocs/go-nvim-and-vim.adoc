= Go (Golang) with NeoVim (nvim) and Vim

=== Social

==== YouTubers

https://youtu.be/7BqJ8dzygtU[Go with Vim-go] by DigitalOcean

https://youtu.be/iSQ0if992k8[Golang Vim Plugins] from https://www.youtube.com/channel/UCTHij3Ac5GizLsn5yB4IX_Q[Donald Feury]

https://youtu.be/k2VpTYctEVU[Debugging]

https://youtu.be/7zCCnT9a58k[Getting started]

https://youtu.be/hHGivMVQXwY[vim-go tweaks]

https://youtu.be/T32yqetyy8s[Vim-go NeoVim LSP] from https://www.youtube.com/c/Octetz/videos[Octetz]



