= Neovim Help links

== Vim/Neovim

https://neovim.io/doc/user/index.html[Neovim help]

https://www.hillelwayne.com/post/intermediate-vim/[Intermediate hints]

== Neovim Lua

https://neovim.io/doc/user/lua.html[The horses mouth, so to speak]

https://github.com/nanotee/nvim-lua-guide[a guide]


=== Init.lua

https://vonheikemen.github.io/devlog/tools/configuring-neovim-using-lua/[Big Neovim Lua Config notes]

https://github.com/VonHeikemen/dotfiles/tree/master/my-configs/neovim[vonheikemen again]

https://oroques.dev/notes/neovim-init/[Olivier Roques nvim 0.5]

https://github.com/Allaman/nvim/[Lua nvim config]

https://github.com/nanotee/nvim-lua-guide[Getting started]

https://www.notonlycode.org/neovim-lua-config/[crash course on init.lua]

https://github.com/David-Kunz/vim/blob/master/init.lua[An example init.lua on github]

https://gist.github.com/benfrain/97f2b91087121b2d4ba0dcc4202d252f/9bb064b019dfcdb131024cfc63fd4deb9e694198[an example]

https://github.com/ojroques/dotfiles/blob/master/nvim/.config/nvim/init.lua[Olivier again?]



=== Lua specifics

https://vi.stackexchange.com/questions/31811/neovim-lua-config-how-to-append-to-listchars[listchars]

https://vi.stackexchange.com/questions/11887/show-newline-characters-in-neovim[listchars but just vim?]

https://www.reddit.com/r/neovim/comments/qke1oe/help_needed_for_initlua/[listchars]

https://www.reddit.com/r/neovim/comments/p6dhrk/set_space_to_leader_key_with_lua/[lua leader space]

https://www.reddit.com/r/neovim/comments/kafpam/how_to_set_mapleader_in_lua/[mapleader space lua]

https://www.reddit.com/r/neovim/comments/qsezd6/how_to_convert_a_bit_of_vimscript_to_lua/[converting vimscript to lua]


=== Neovim config

https://gist.github.com/subfuzion/7d00a6c919eeffaf6d3dbf9a4eb11d64[example]


== Plugins

=== moving around

EasyMotion or Sneak
https://www.barbarianmeetscoding.com/boost-your-coding-fu-with-vscode-and-vim/moving-even-faster-with-vim-sneak-and-easymotion/[vim-sneak vim-EasyMotion]

https://github.com/easymotion/vim-easymotion[EasyMotion github]

https://www.reddit.com/r/vim/comments/2ydw6t/large_plugins_vs_small_easymotion_vs_sneak/[Reddit compares]

https://www.reddit.com/r/neovim/comments/ksgu5r/modern_alternatives_for_vimeasymotion_plugin/[alternatives to EasyMotion Reddit]


https://www.reddit.com/r/vim/comments/hrlric/mapping_for_jumping_from_terminal_to_other_split/[move between splits and term]

https://github.com/neovim/neovim/issues/5504[can have nvim C-h etc problems]

== Ex mode

https://www.reddit.com/r/vim/comments/6z9i5j/example_of_vims_exmode_magic_that_can_make_you/[reddit]

https://vi.stackexchange.com/questions/457/does-ex-mode-have-any-practical-use[stackexchange]


== Neovim mapping

https://neovim.io/doc/user/map.html[More from Neovim.io]

=== Normal

https://learnvimscriptthehardway.stevelosh.com/chapters/29.html[Vimscript the hard way]

https://vim.fandom.com/wiki/Using_normal_command_in_a_script_for_searching[Vim Tips Wiki]

https://stackoverflow.com/questions/41538389/vim-mapping-normal-mode-key-sequences[SO mapping normal mode keys sequences]


https://strftime.org/[strftime to print time and date in vim]

https://www.reddit.com/r/vim/comments/4ofv82/the_normal_command_is_really_cool/[Normal in Reddit]

=== File locations

https://stackoverflow.com/questions/48700563/how-do-i-install-plugins-in-neovim-correctly[Copied from this SO]

Both VIm 8.0 and Neovim have their own built-in package manager.

In **VIm 8.0**, create the following directories:

 - `~/.vim/pack/*/start` (where `*` may be any name _e.g._ `~/.vim/pack/jimmy/start`): Clone your required plugin into the `start` directory just as you would if you were installing it for Pathogen. You need nothing more and no commands in your `.vimrc` file.
 - `~/.vim/pack/*/opt` (_e.g._ `~/.vim/pack/jimmy/opt`) for plugins that only need to be loaded as required. For colours, add a directory to the `opt` directory and then your colours _e.g._ `~/.vim/pack/jimmy/opt/mycolors/colors/dracula.vim`.

In **Neovim**, the directory structure follows the freedesktop's [XDG Base Directory Specification](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html). Your configuration file is in `~/.config/nvim/init.vim`, but your plugins go into:

* `~/.local/share/nvim/site/pack/*/start`

See `:h packages` (VIm 8.0 and Neovim) for more information.

== Reload rc and check variables

https://www.cyberciti.biz/faq/how-to-reload-vimrc-file-without-restarting-vim-on-linux-unix/[reload]

https://superuser.com/questions/132029/how-do-you-reload-your-vimrc-file-without-restarting-vim[superuser again]

https://stackoverflow.com/questions/9193066/how-do-i-inspect-vim-variables[inspect variables]

https://vim.fandom.com/wiki/Displaying_the_current_Vim_environment[display the env]


== Vim standard stuff
https://stackoverflow.com/questions/53664/how-to-effectively-work-with-multiple-files-in-vim[Tabs]

https://blog.confirm.ch/mastering-vim-working-with-multiple-files/[Multiple files]
